package com.example.tanvi.roomdatabase.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.tanvi.roomdatabase.MainActivity;
import com.example.tanvi.roomdatabase.R;
import com.example.tanvi.roomdatabase.Database.User;

/**
 * Created by tanvi on 30/3/18.
 */

public class DeleteUserFragment extends Fragment {
    private EditText mUserId;
    private Button mButtonDelete;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_delete_user, container, false);
        mUserId = view.findViewById(R.id.edt_user_id);
        mButtonDelete = view.findViewById(R.id.button_delete);

        mButtonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User();
                user.setId(Integer.parseInt(mUserId.getText().toString()));

                MainActivity.appDatabase.getUserDao().deleteUser(user);

                mUserId.setText("");
            }
        });
        return view;
    }

    public DeleteUserFragment() {
    }
}
