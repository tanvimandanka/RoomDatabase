package com.example.tanvi.roomdatabase.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.tanvi.roomdatabase.MainActivity;
import com.example.tanvi.roomdatabase.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * <p>
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {
    private Button button_add_user, button_view_user, button_delete_user,button_update_user;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        button_add_user = view.findViewById(R.id.button_add_user);
        button_view_user = view.findViewById(R.id.button_view_user);
        button_delete_user = view.findViewById(R.id.button_delete_user);
        button_update_user = view.findViewById(R.id.button_update_user);
        button_add_user.setOnClickListener(this);
        button_view_user.setOnClickListener(this);
        button_delete_user.setOnClickListener(this);
        button_update_user.setOnClickListener(this);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_add_user:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.layout_framelayout, new AddUserFragment())
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.button_view_user:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.layout_framelayout, new ReadUserFragment())
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.button_delete_user:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.layout_framelayout, new DeleteUserFragment())
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.button_update_user:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.layout_framelayout, new UpdateUserFragment())
                        .addToBackStack(null)
                        .commit();
                break;

        }
    }

}
