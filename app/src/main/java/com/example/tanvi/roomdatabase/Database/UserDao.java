package com.example.tanvi.roomdatabase.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by tanvi on 30/3/18.
 */

@Dao
public interface UserDao {

    @Insert
    public void addUser(User user);

    @Query("Select * from users")
    public List<User> getUsers();

    @Delete
    public void deleteUser(User user);

    @Update
    public void updateUser(User user);


}
